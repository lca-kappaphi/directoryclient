import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BrotherListComponent }   from './components/index';
import { BrotherDetailComponent }  from './components/index';

const routes: Routes = [
  { path: '',  component: BrotherListComponent },
  { path: 'details/:id', component: BrotherDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}