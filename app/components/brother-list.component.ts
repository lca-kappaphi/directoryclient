import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { Brother } from "../models/index";
import { BrotherService } from "../services/index";

@Component({
    selector: "brother-list",
    templateUrl: "app/components/brother-list.template.html",
    styleUrls: [
        "/assets/main.css",
        "/assets/normalize.css"
    ]
})
export class BrotherListComponent implements OnInit {
      brothers: Brother[][];
      selectedBrother: Brother;

  constructor(private brotherService: BrotherService, private router: Router){
  }

  ngOnInit(){
    this.loadBrothers();
  }

  initializeGrid(b: Brother[]){
    let x: number = 0;
    this.brothers = Array(Math.floor(b.length/3));
    for(let i = 0; i < b.length; i++){
      this.brothers[i] = Array(3);
      for(let r = 0; r < 3 && i < b.length; r++){
        this.brothers[i][r] = b[x++];
      }
    }
  }

  loadBrothers(){
    this.brotherService.getBrothers().subscribe(
      b => this.initializeGrid(b),
      err => {
        console.error(err);
      }
    )
  }

  search(){
    let searchText = "";
    let element = <HTMLInputElement>document.getElementById("search-field");

    if(element){
      searchText = element.value;
    }

    if(searchText == ""){
      this.loadBrothers();
    }else{
      this.brotherService.searchBrothers(searchText).subscribe(
        b => this.initializeGrid(b),
        err => console.error(err)
      );
    }
  }

  onSelect(brother: Brother){
    this.selectedBrother = brother;
    this.goToDetail();
  }

  goToDetail(): void{
    this.router.navigate(['/details', this.selectedBrother.ID]);
  }
  
  getYear(): number{
      return new Date().getFullYear();
  }
}