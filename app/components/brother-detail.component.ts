import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { Location } from "@angular/common";
import 'rxjs/add/operator/switchMap';

import { Brother } from "../models/index";

import { BrotherService } from "../services/index";

@Component({
    moduleId: module.id,
    selector: 'brother-detail',
    templateUrl: 'brother-detail.template.html',
    styleUrls: [
        "/assets/main.css",
        "/assets/normalize.css"
    ]
})
export class BrotherDetailComponent implements OnInit {
    brother: Brother;

    constructor(private brotherService: BrotherService, private route: ActivatedRoute, private location: Location){
    }

    ngOnInit(): void {
        this.route.params
        .switchMap((p: Params) => this.brotherService.getBrother(+p['id']))
        .subscribe(b => {
            this.brother = b;

            //properly parse dates since typescript is for some reason ok with having dates actually be strings and not dates
            //this will avoid extra parsing for no reason later
            this.brother.DateJoined = new Date(this.brother.DateJoined);
            this.brother.DateInitiated = new Date(this.brother.DateInitiated);
            this.brother.ExpectedGraduation = new Date(this.brother.ExpectedGraduation);
        });
    }

    getGraduationTense(): string{
        if(new Date().getTime() > this.brother.ExpectedGraduation.getTime()){
            return "Graduated";
        }
        return "Expected graduation:";
    }

    convertDateIntoSemester(date: Date): string{
        let d = "";
        if(date.getMonth() <= 5){
            d = "Spring";
        }else if(date.getMonth() >= 10){
            d = "Fall";
        }else{
            d = "Summer";
        }

        return d + " " + date.getFullYear();
    }

    formatList(s: string[]): string{
        let commas = s.length - 1;
        let majors = "";

        if(s.length < 1){
            return "";
        }
        if(s.length == 1){
            return s[0];
        }

        s.forEach(el => {
            majors += el + (commas-- > 0 ? ", " : "");
        });

        return majors;
    }

    getMajors(): string{
        if(!this.brother.Majors || this.brother.Majors.length == 0 || this.brother.Majors[0].toLowerCase().startsWith("undecided")){
            return "No major (undecided)";
        }
        return this.formatList(this.brother.Majors) + " major";
    }

    getMinors(): string{
        let d: string;
        if(!this.brother.Minors || this.brother.Minors.length == 0){
            return "No minor";
        }
        return this.formatList(this.brother.Minors) + " minor";
    }

    getCurrentPositions(): string{
        if(!this.brother.CurrentPositions || this.brother.CurrentPositions.length == 0){
            return "Not currently holding any positions";
        }
        return "Currently holding " + this.formatList(this.brother.CurrentPositions);
    }

    getPastPositions(): string{
        if(!this.brother.PastPositions || this.brother.PastPositions.length == 0){
            return "No offices held in the past";
        }
        return "Previously held " + this.formatList(this.brother.PastPositions);
    }

    determinePlurality(s: any[], singular:string, plural: string){
        //0 is plural
        if(s.length == 1){
            return singular;
        }
        return plural
    }

    formatDate(d: Date): string{
        return d.toLocaleDateString("en-US");
    }

    goBack(): void {
        this.location.back();
    }

    getYear(): number{
        return new Date().getFullYear();
    }
}