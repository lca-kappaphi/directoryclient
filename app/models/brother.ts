import { Question } from "./index";

export class Brother{
    ID: number;
    Name: string;
    DateJoined: Date;
    DateInitiated: Date;
    ExpectedGraduation: Date;
    ZetaNumber: number;
    BigBrother: string;
    LittleBrothers: string[];
    Majors: string[];
    Minors: string[];
    CurrentPositions: string[];
    PastPositions: string[];
    Questions: Question[];
}