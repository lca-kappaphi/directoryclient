import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "directory",
    templateUrl: "app.template.html"
})
export class AppComponent { }