import { NgModule }       from '@angular/core';
import { APP_BASE_HREF }   from '@angular/common';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }     from '@angular/http';
import { Observable } from "rxjs/Observable";

import { AppComponent } from "./app.component";
import { Brother } from "./models/index";
import { BrotherService } from "./services/index";

import { BrotherListComponent, BrotherDetailComponent }  from './components/index';
import { AppRoutingModule } from "./app-routing.module";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    BrotherListComponent,
    BrotherDetailComponent
  ],
  providers: [
    {
      provide: APP_BASE_HREF, useValue: '/'
    },
    BrotherService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
