import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions, Response } from "@angular/http";

import { Brother } from "../models";

import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';

const RETRIES: number = 1;

@Injectable()
export class BrotherService {
    private uri: string;
    

    constructor(private http: Http){
        this.uri = "https://kappa-phi.com/brothers-api/api";
    }

    public getApiUri(): string{
        return this.uri;
    }

    getBrothers(): Observable<Brother[]>{
        return this.http.get(`${this.uri}/Brothers`)
        .retry(RETRIES)
        .catch(this.catchErrors)
        .map((resp: Response, i: number) => resp.json().Data);
    }

    getBrother(id: number): Observable<Brother>{
        return this.http.get(`${this.uri}/Brothers/${id}`)
        .retry(RETRIES)
        .catch(this.catchErrors)
        .map((resp: Response, index: number) => resp.json());
    }

    searchBrothers(search: string): Observable<Brother[]>{
        return this.http.get(`${this.uri}/Search/${search}`)
        .retry(RETRIES)
        .catch(this.catchErrors)
        .map((r: Response, i: number) => r.json().Data);
    }

    private catchErrors(error: any){
        let errorMessage: string;
        if(error.message){
            errorMessage = error.message;
        }else if(error.status){
            errorMessage = `${error.status} - ${error.statusText}`;
        }else{
            errorMessage = "HTTP Error.";
        }

        console.log(errorMessage);
        return Observable.throw(errorMessage);
    }
}